using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text.Json;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using System.Net.Http;
using System.Reflection;
using System.Diagnostics;


namespace Cygnia.Integration.Shopify
{
    public class BouclemeVirtualItem
    {
        public string Item { get; set; }
    }

    public class CygniaStockRecord
    {
        public string Item { get; set; }
        public Stock StockRecord { get; set; }
    }

    public class CygniaAdjustmentRecord
    {
        public string Item { get; set; }
        public InventoryLevelSet StockRecord { get; set; }
    }

    public class InventoryResponse
    {
        public Inventory_Level inventory_level { get; set; }
    }

    public class Inventory_Level
    {
        public long inventory_item_id { get; set; }
        public long location_id { get; set; }
        public int available { get; set; }
        public DateTime? updated_at { get; set; }
        public string admin_graphql_api_id { get; set; }
    }


    public class InventoryLevelSet
    {
        public long location_id { get; set; }
        public long inventory_item_id { get; set; }
        public long available_adjustment { get; set; }
    }


    public class Stock
    {
        public long location_id { get; set; }
        public long inventory_item_id { get; set; }
        public int available { get; set; }
        public bool disconnect_if_necessary { get; set; }
    }

    public class ItemCrossMatch
    {
        public string Item { get; set; }
        public string Sku { get; set; }
        public long ID { get; set; }
        public long ProductID { get; set; }
    }

    public class Inventory
    {
        public string Item { get; set; }
        public int Available { get; set; }
        public int OnHand { get; set; }
        public int Demand { get; set; }

        public string Status { get; set; }
        public string ID { get; set; }

        public int Adjust { get; set; }

    }
    public class Products
    {
        public List<Product> products { get; set; }
    }

    public class Product
    {
        public long id { get; set; }
        public string title { get; set; }
        public string body_html { get; set; }
        public string vendor { get; set; }
        public string product_type { get; set; }
        public DateTime? created_at { get; set; }
        public string handle { get; set; }
        public DateTime? updated_at { get; set; }
        public DateTime? published_at { get; set; }
        public string template_suffix { get; set; }
        public string published_scope { get; set; }
        public string tags { get; set; }
        public string admin_graphql_api_id { get; set; }
        public Variant[] variants { get; set; }
        public Option[] options { get; set; }
        public Image1[] images { get; set; }
        public Image image { get; set; }
        public string status { get; set; }
    }

    public class Image
    {
        public long id { get; set; }
        public long product_id { get; set; }
        public int position { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public object alt { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string src { get; set; }
        public object[] variant_ids { get; set; }
        public string admin_graphql_api_id { get; set; }
    }

    public class Variant
    {
        public long id { get; set; }
        public long product_id { get; set; }
        public string title { get; set; }
        public string price { get; set; }
        public string sku { get; set; }
        public int position { get; set; }
        public string inventory_policy { get; set; }
        public object compare_at_price { get; set; }
        public string fulfillment_service { get; set; }
        public string inventory_management { get; set; }
        public string option1 { get; set; }
        public object option2 { get; set; }
        public object option3 { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public bool taxable { get; set; }
        public string barcode { get; set; }
        public int grams { get; set; }
        public object image_id { get; set; }
        public float weight { get; set; }
        public string weight_unit { get; set; }
        public long inventory_item_id { get; set; }
        public int inventory_quantity { get; set; }
        public int old_inventory_quantity { get; set; }
        public bool requires_shipping { get; set; }
        public string admin_graphql_api_id { get; set; }
    }

    public class Option
    {
        public long id { get; set; }
        public long product_id { get; set; }
        public string name { get; set; }
        public int position { get; set; }
        public string[] values { get; set; }
    }

    public class Image1
    {
        public long id { get; set; }
        public long product_id { get; set; }
        public int position { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public object alt { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string src { get; set; }
        public object[] variant_ids { get; set; }
        public string admin_graphql_api_id { get; set; }
    }


    public class Baton
    {
        public Batonupdate BatonUpdate { get; set; }
    }

    public class Batonupdate
    {
        public string AppName { get; set; }
        public DateTime? LastStarted { get; set; }
        public DateTime? LastEnded { get; set; }
        public DateTime? ExpectedNext { get; set; }
        public int WarningStatus { get; set; }
    }


    public class CygniaShopifyDespatch
    {
        public string ShipID { get; set; }
        public string OrderID { get; set; }
        public int InternalID { get; set; }
        public decimal Weight { get; set; }
        public string TrackID { get; set; }
        public string Carrier { get; set; }
        public string ShipPostCode { get; set; }
        public string Item { get; set; }
        public string ItemID { get; set; }
        public int Quantity { get; set; }
        public int NewOrder { get; set; }

    }

    public class API
    {
        public string Client { get; set; }
        public string Prefix { get; set; }
        public string URL { get; set; }
        public string APIKey { get; set; }
        public string APIPassword { get; set; }
        public string APISharedSecret { get; set; }
        public string APICategory9 { get; set; }
        public DateTime LastRun { get; set; }
        public string TimeStampID { get; set; }
        public int ID { get; set; }

        public string LocationID { get; set; }
        public List<Carrier> Carriers { get; set; }
        public string DespatchSP { get; set; }

        public string StockSP { get; set; }
        public string AdjustSP { get; set; }
        public DateTime NextShip { get; set; }
    }

    public class Country
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string EU { get; set; }
    }

    public class Carrier
    {
        public string APICarrier { get; set; }
        public string CarrierName { get; set; }

        public string CarrierService { get; set; }
        public string CarrierType { get; set; }
        public double CarrierValueThreshold { get; set; }
    }

    public class TrackingURLs
    {
        public string Carrier { get; set; }
        public string URL { get; set; }
    }


    public static class Hub
    {
        public static List<TrackingURLs> TrackingLinks = null;
        public static List<string> OrdersAlreadyDownloaded = null;
        public static List<Country> Countries = null;

        [FunctionName("StockAdjust")]
        public static void RunStockAdjust([TimerTrigger("%TriggerTimeRunStockAdjust%")] TimerInfo myTimer, ILogger log)
        {
            List<API> apis = new List<API>();
            string strGlobeConn = Environment.GetEnvironmentVariable("ConnGlobe");
            //log.LogInformation(strGlobeConn);

            try
            {
                using (SqlConnection con = new SqlConnection(strGlobeConn))
                {
                    log.LogInformation("Connecting: {0}", con.Database);
                    con.Open();
                    log.LogInformation("Connected");

                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "API_SHOPIFY";
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@ENVIRONMENT", Environment.GetEnvironmentVariable("CygniaEnvironment"));

                        SqlDataReader apiReader = comm.ExecuteReader();

                        while (apiReader.Read())
                        {
                            API a = new API()
                            {
                                Client = apiReader["Company"].ToString(),
                                Prefix = apiReader["Prefix"].ToString(),
                                URL = apiReader["URL"].ToString(),
                                APIKey = apiReader["Key"].ToString(),
                                APIPassword = apiReader["Password"].ToString(),
                                APISharedSecret = apiReader["SharedSecret"].ToString(),
                                APICategory9 = apiReader["Category9"].ToString(),
                                LastRun = Convert.ToDateTime(apiReader["LastRun"]),
                                TimeStampID = apiReader["LookupID"].ToString(),
                                ID = Convert.ToInt32(apiReader["ID"].ToString().Replace(".00000", "")),
                                DespatchSP = apiReader["DespatchSP"].ToString(),
                                LocationID = apiReader["LocationID"].ToString(),
                                StockSP = apiReader["StockSP"].ToString(),
                                AdjustSP = apiReader["AdjustSP"].ToString()
                            };

                            apis.Add(a);
                        }

                        apiReader.Close();

                    }

                    con.Close();

                }

            }
            catch (Exception ex)
            {
                log.LogInformation("RunStock: " + ex.Message);
            }
            finally
            {

            }

            foreach (API a in apis)
            {
                DateTime LastRun = DateTime.Now;

                log.LogInformation(a.Client);
                log.LogInformation(a.URL);

                if (a.AdjustSP != "")
                {
                    StockAdjustUpdate(a, log);
                }

                //StockUpdate(a, log);
                //UploadOrders(a, log);
                //UpdateTimeStamps(a, log, LastRun);
            }
        }

        [FunctionName("Stock")]
        public static void RunStock([TimerTrigger("%TriggerTimeRunStock%")] TimerInfo myTimer, ILogger log)
        {
            NetworkTest(log);

            List<API> apis = new List<API>();
            string strGlobeConn = Environment.GetEnvironmentVariable("ConnGlobe");
            //log.LogInformation(strGlobeConn);

            try
            {
                using (SqlConnection con = new SqlConnection(strGlobeConn))
                {
                    log.LogInformation("Connecting: {0}", con.Database);
                    con.Open();
                    log.LogInformation("Connected");

                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "API_SHOPIFY";
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@ENVIRONMENT", Environment.GetEnvironmentVariable("CygniaEnvironment"));

                        SqlDataReader apiReader = comm.ExecuteReader();

                        while (apiReader.Read())
                        {
                            API a = new API()
                            {
                                Client = apiReader["Company"].ToString(),
                                Prefix = apiReader["Prefix"].ToString(),
                                URL = apiReader["URL"].ToString(),
                                APIKey = apiReader["Key"].ToString(),
                                APIPassword = apiReader["Password"].ToString(),
                                APISharedSecret = apiReader["SharedSecret"].ToString(),
                                APICategory9 = apiReader["Category9"].ToString(),
                                LastRun = Convert.ToDateTime(apiReader["LastRun"]),
                                TimeStampID = apiReader["LookupID"].ToString(),
                                ID = Convert.ToInt32(apiReader["ID"].ToString().Replace(".00000", "")),
                                DespatchSP = apiReader["DespatchSP"].ToString(),
                                LocationID = apiReader["LocationID"].ToString(),
                                StockSP = apiReader["StockSP"].ToString()
                            };

                            apis.Add(a);
                        }

                        apiReader.Close();

                    }

                    con.Close();

                }

            }
            catch (Exception ex)
            {
                log.LogInformation("RunStock: " + ex.Message);
            }
            finally
            {

            }

            foreach (API a in apis)
            {
                DateTime LastRun = DateTime.Now;

                log.LogInformation(a.Client);
                log.LogInformation(a.URL);

                if (a.StockSP != "")
                {
                    StockUpdate(a, log);
                }

                //UploadOrders(a, log);
                //UpdateTimeStamps(a, log, LastRun);
            }

        }

        public static void StockAdjustUpdate(API a, ILogger log)
        {
            List<ItemCrossMatch> products = StockGetProducts(a, log);

            if (products.Count > 0)
            {
                string strScaleConn = Environment.GetEnvironmentVariable("ConnScale");
                //log.LogInformation(strGlobeConn);

                List<Inventory> stock = new List<Inventory>();
                List<CygniaAdjustmentRecord> stockRecords = new List<CygniaAdjustmentRecord>();

                try
                {
                    using (SqlConnection con = new SqlConnection(strScaleConn))
                    {
                        log.LogInformation("Connecting: {0}", con.Database);
                        con.Open();
                        log.LogInformation("Connected");

                        using (SqlCommand comm = con.CreateCommand())
                        {
                            comm.CommandText = a.AdjustSP;
                            comm.CommandType = System.Data.CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@COMPANY", a.Client);

                            SqlDataReader stockReader = comm.ExecuteReader();

                            while (stockReader.Read())
                            {

                                Inventory i = new Inventory()
                                {
                                    Item = stockReader["Item"].ToString(),
                                    Adjust = Convert.ToInt32(stockReader["QUANTITY"].ToString().Replace(".00000", ""))
                                };

                                ItemCrossMatch c = products.Find(x => x.Item == i.Item);

                                if (c != null)
                                {
                                    i.ID = c.ID.ToString();
                                    InventoryLevelSet s = new InventoryLevelSet()
                                    {
                                        inventory_item_id = Convert.ToInt64(i.ID),
                                        available_adjustment = i.Adjust,
                                        location_id = Convert.ToInt64(a.LocationID),
                                    };
                                    CygniaAdjustmentRecord sr = new CygniaAdjustmentRecord()
                                    {
                                        Item = i.Item,
                                        StockRecord = s
                                    };

                                    stockRecords.Add(sr);
                                }
                                else
                                {
                                    log.LogInformation(a.Client + " Cross Reference Not Found: " + i.Item);
                                }


                            }

                            stockReader.Close();
                        }

                        con.Close();

                        StockAdjustSubmit(a, stockRecords, log);

                    }
                }
                catch (Exception ex)
                {
                    log.LogInformation("Stock Connection Error: " + ex.Message);
                }

            }
            else
            {
                log.LogInformation("No cross references found");
            }

        }

        public static void StockUpdate(API a, ILogger log)
        {

            List<ItemCrossMatch> products = StockGetProducts(a, log);

            if (products.Count > 0)
            {
                string strScaleConn = Environment.GetEnvironmentVariable("ConnScale");
                //log.LogInformation(strGlobeConn);

                List<Inventory> stock = new List<Inventory>();
                List<CygniaStockRecord> stockRecords = new List<CygniaStockRecord>();

                try
                {
                    using (SqlConnection con = new SqlConnection(strScaleConn))
                    {
                        log.LogInformation("Connecting: {0}", con.Database);
                        con.Open();
                        log.LogInformation("Connected");

                        using (SqlCommand comm = con.CreateCommand())
                        {
                            comm.CommandText = a.StockSP;
                            comm.CommandType = System.Data.CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@COMPANY", a.Client);

                            SqlDataReader stockReader = comm.ExecuteReader();

                            while (stockReader.Read())
                            {

                                Inventory i = new Inventory()
                                {
                                    Item = stockReader["Item"].ToString(),
                                    OnHand = Convert.ToInt32(stockReader["OnHand"].ToString().Replace(".00000", "")),
                                    Available = Convert.ToInt32(stockReader["Available"].ToString().Replace(".00000", "")),
                                    Demand = Convert.ToInt32(stockReader["Demand"].ToString().Replace(".00000", "")),
                                };

                                List<ItemCrossMatch> c = products.FindAll(x => x.Item == i.Item);

                                if (c != null)
                                {
                                    foreach (ItemCrossMatch ic in c)
                                    {
                                        i.ID = ic.ID.ToString();
                                        Stock s = new Stock()
                                        {
                                            inventory_item_id = Convert.ToInt64(i.ID),
                                            available = i.Available,
                                            location_id = Convert.ToInt64(a.LocationID),
                                        };
                                        CygniaStockRecord sr = new CygniaStockRecord()
                                        {
                                            Item = i.Item,
                                            StockRecord = s
                                        };

                                        stockRecords.Add(sr);
                                    }
                                }
                                else
                                {
                                    log.LogInformation(a.Client + " Cross Reference Not Found: " + i.Item);
                                }


                            }

                            stockReader.Close();
                        }

                        con.Close();

                        StockSubmit(a, stockRecords, log);

                    }
                }
                catch (Exception ex)
                {
                    log.LogInformation("Stock Connection Error: " + ex.Message);
                }
            }
            else
            {
                log.LogInformation("No cross references found");
            }
        }

        public static void StockAdjustSubmit(API a, List<CygniaAdjustmentRecord> stockRecords, ILogger log)
        {
            foreach (CygniaAdjustmentRecord s1 in stockRecords)
            {
                InventoryLevelSet s = s1.StockRecord;

                string postData = JsonSerializer.Serialize(s);
                byte[] bytes = Encoding.UTF8.GetBytes(postData);
                string strAddress = URL(a, "StockAdjust", 0, "");

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                WebRequest request = WebRequest.Create(strAddress);
                request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(a.APIKey + ":" + a.APIPassword));
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";

                try
                {
                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(bytes, 0, bytes.Count());
                    }
                    var response = (HttpWebResponse)request.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);

                    string responseFromServer = reader.ReadToEnd();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string GoodResp = responseFromServer;
                        InventoryResponse r = JsonSerializer.Deserialize<InventoryResponse>(responseFromServer);
                        log.LogInformation("Good Update: " + s1.Item + " - " + s.available_adjustment.ToString() + " - " + r.inventory_level.available.ToString());
                    }
                    else
                    {
                        log.LogInformation("Bad Update: " + s1.Item + " " + responseFromServer);
                        //Email.Add(dRead["Item"].ToString() + "~Item Stock Set Error~" + responseFromServer.ToString());
                    }

                    System.Threading.Thread.Sleep(501);

                }
                catch (Exception ex)
                {
                    log.LogInformation("Error Update: " + s1.Item + " " + ex.Message);
                }
            }
        }

        public static void StockSubmit(API a, List<CygniaStockRecord> stockRecords, ILogger log)
        {
            foreach (CygniaStockRecord s1 in stockRecords)
            {
                Stock s = s1.StockRecord;

                string postData = JsonSerializer.Serialize(s);
                byte[] bytes = Encoding.UTF8.GetBytes(postData);
                string strAddress = URL(a, "Stock", 0, "");

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                WebRequest request = WebRequest.Create(strAddress);
                request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(a.APIKey + ":" + a.APIPassword));
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = "application/json; charset=utf-8";

                try
                {
                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(bytes, 0, bytes.Count());
                    }
                    var response = (HttpWebResponse)request.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);

                    string responseFromServer = reader.ReadToEnd();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string GoodResp = responseFromServer;
                        InventoryResponse r = JsonSerializer.Deserialize<InventoryResponse>(responseFromServer);
                        log.LogInformation("Good Update: " + s1.Item + " - " + r.inventory_level.available.ToString());
                    }
                    else
                    {
                        log.LogInformation("Bad Update: " + s1.Item + " " + responseFromServer);
                        //Email.Add(dRead["Item"].ToString() + "~Item Stock Set Error~" + responseFromServer.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.LogInformation("Error Update: " + s1.Item + " " + ex.Message);
                }

                System.Threading.Thread.Sleep(500);

            }
        }

        public static List<ItemCrossMatch> StockGetProducts(API a, ILogger log)
        {
            int productLimit = Convert.ToInt32(Environment.GetEnvironmentVariable("ProductLimit"));

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            Products products = null;

            List<ItemCrossMatch> items = new List<ItemCrossMatch>();
            Common.LinkHeader l = null;

            string strURL = URL(a, "Products", productLimit, "");

            while (strURL != "" && strURL.ToLower().Contains("http"))
            {
                WebRequest request = WebRequest.Create(strURL);

                request.Credentials = new NetworkCredential(a.APIKey, a.APIPassword);

                log.LogInformation(request.RequestUri.AbsoluteUri);

                HttpWebResponse response = null;
                try
                {
                    response = (HttpWebResponse)request.GetResponse();

                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    string responseFromServer = reader.ReadToEnd();

                    if (response.StatusDescription == "OK")
                    {

                        l = Common.LinkHeader.LinksFromHeader(response.Headers["link"]);
                        if (l != null)
                        {
                            if (l.NextLink != null)
                            {
                                strURL = l.NextLink;
                            }
                            else
                            {
                                strURL = "";
                            }
                        }
                        else
                        {
                            strURL = "";
                        }

                        products = JsonSerializer.Deserialize<Products>(responseFromServer);

                        foreach (Product p in products.products)
                        {
                            Variant[] vars = p.variants;

                            foreach (Variant itv in vars)
                            {
                                ItemCrossMatch it = new ItemCrossMatch();
                                it.Item = itv.sku;
                                it.ProductID = itv.product_id;
                                it.ID = itv.inventory_item_id;

                                items.Add(it);
                            }
                        };

                    }

                    reader.Close();
                    dataStream.Close();
                    response.Close();

                }
                catch (Exception ex)
                {
                    log.LogInformation("StockGetProducts: " + ex.Message);
                    strURL = ex.Message;
                }
            }
            return items;
        }



        [FunctionName("Despatch")]
        public static void RunDespatch([TimerTrigger("%TriggerTimeRunDespatch%")] TimerInfo myTimer, ILogger log)
        {
            NetworkTest(log);

            List<API> apis = new List<API>();
            TrackingLinks = new List<TrackingURLs>(); Environment.GetEnvironmentVariable("ConnGlobe");

            string strGlobeConn = Environment.GetEnvironmentVariable("ConnGlobe");
            //log.LogInformation(strGlobeConn);

            try
            {
                using (SqlConnection con = new SqlConnection(strGlobeConn))
                {
                    log.LogInformation("Connecting: {0}", con.Database);
                    con.Open();
                    log.LogInformation("Connected");

                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "API_SHOPIFY";
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@ENVIRONMENT", Environment.GetEnvironmentVariable("CygniaEnvironment"));

                        SqlDataReader apiReader = comm.ExecuteReader();

                        while (apiReader.Read())
                        {
                            API a = new API()
                            {
                                Client = apiReader["Company"].ToString(),
                                Prefix = apiReader["Prefix"].ToString(),
                                URL = apiReader["URL"].ToString(),
                                APIKey = apiReader["Key"].ToString(),
                                APIPassword = apiReader["Password"].ToString(),
                                APISharedSecret = apiReader["SharedSecret"].ToString(),
                                APICategory9 = apiReader["Category9"].ToString(),
                                LastRun = Convert.ToDateTime(apiReader["LastRun"]),
                                TimeStampID = apiReader["LookupID"].ToString(),
                                ID = Convert.ToInt32(apiReader["ID"].ToString().Replace(".00000", "")),
                                DespatchSP = apiReader["DespatchSP"].ToString(),
                                LocationID = apiReader["LocationID"].ToString()
                            };

                            a.LastRun = DateTime.Now.AddMonths(-3);

                            apis.Add(a);
                        }

                        apiReader.Close();

                    }



                    con.Close();

                }

            }
            catch (Exception ex)
            {
                log.LogInformation("RunDespatch: " + ex.Message);
            }
            finally
            {

            }

            foreach (API a in apis)
            {
                DateTime LastRun = DateTime.Now;

                log.LogInformation(a.Client);
                log.LogInformation(a.URL);
                UploadOrders(a, log);
                //UpdateTimeStamps(a, log, LastRun);
            }

        }

        public static void GetTrackingURLs(ILogger log)
        {
            if (TrackingLinks.Count == 0)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(Environment.GetEnvironmentVariable("ConnGlobe")))
                    {
                        con.Open();

                        using (SqlCommand comm = con.CreateCommand())
                        {
                            comm.CommandText = "API_CARRIERS_URL";
                            comm.CommandType = System.Data.CommandType.StoredProcedure;

                            SqlDataReader trackReader = comm.ExecuteReader();

                            while (trackReader.Read())
                            {
                                TrackingURLs t = new TrackingURLs()
                                {
                                    Carrier = trackReader["Carrier"].ToString(),
                                    URL = trackReader["URL"].ToString()
                                };

                                TrackingLinks.Add(t);
                            }

                            trackReader.Close();
                            trackReader = null;
                        }
                        con.Close();
                    }

                }
                catch (Exception ex)
                {
                    log.LogInformation("Tracking URLs: " + ex.Message);
                }
            }
        }

        public static void UploadOrders(API a, ILogger log)
        {
            //List<OrderConfirm> confirms = new List<OrderConfirm>();

            List<CygniaShopifyDespatch> desps = UploadOrdersGet(a, log);

            if (desps.Count > 0)
            {
                GetTrackingURLs(log);
                //List<ShopifyFulfillment> confs = UploadOrdersGetShopify(a, log, desps);
                List<OrderConfirm.Fulfillment> confs = UploadOrdersGetShopify(a, log, desps);

            }


        }

        public static string UploadOrdersToShopify(API a, ILogger log, OrderConfirm.Fulfillment order, CygniaShopifyDespatch ord)
        {
            OrderConfirm.ConfirmFile nd = new OrderConfirm.ConfirmFile();
            nd.fulfillment = order;

            string strPrefix = ord.ShipID.Substring(0, 3);
            string strOrder = ord.ShipID.Replace(strPrefix, "");

            string strURL = URL(a, "Despatch", 0, ord.OrderID);

            string postData = JsonSerializer.Serialize<OrderConfirm.ConfirmFile>(nd);
            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(strURL);

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            WebRequest request = WebRequest.Create(strURL);
            request.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(a.APIKey + ":" + a.APIPassword));
            request.Method = "POST";
            request.ContentLength = bytes.Length;
            request.ContentType = "application/json; charset=utf-8";

            try
            {

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Count());
                }
                var response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);

                string responseFromServer = reader.ReadToEnd();

                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Created)
                {

                    log.LogInformation("Good Response: " + responseFromServer);

                    using (SqlConnection con = new SqlConnection(Environment.GetEnvironmentVariable("ConnScale")))
                    {
                        con.Open();

                        using (SqlCommand comm = con.CreateCommand())
                        {
                            comm.CommandText = "custom_api_shopify_despatch_confirm";
                            comm.CommandType = System.Data.CommandType.StoredProcedure;
                            comm.Parameters.AddWithValue("@InternalShipmentNum", ord.InternalID);
                            comm.ExecuteNonQuery();
                        }

                        con.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                log.LogInformation("Upload Orders To Shopify: " + ex.Message);

                using (SqlConnection con = new SqlConnection(Environment.GetEnvironmentVariable("ConnScale")))
                {
                    con.Open();

                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "custom_api_shopify_despatch_confirm";
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@InternalShipmentNum", ord.InternalID);
                        comm.Parameters.AddWithValue("@REF", "Failed");
                        comm.ExecuteNonQuery();
                    }

                    con.Close();
                }
            }

            return "";
        }

        public static string GetTrackingLink(CygniaShopifyDespatch o)
        {
            string strReturn = "";
            TrackingURLs t = TrackingLinks.Find(x => x.Carrier == o.Carrier);

            if (t != null)
            {
                strReturn = t.URL.Replace("<CONSIGN>", o.TrackID).Replace("<POSTCODE>", o.ShipPostCode);
            }

            return strReturn;
        }

        public static List<OrderConfirm.Fulfillment> UploadOrdersGetShopify(API a, ILogger log, List<CygniaShopifyDespatch> desps)
        {
            List<OrderConfirm.Fulfillment> cons = new List<OrderConfirm.Fulfillment>();

            var orders = desps.GroupBy(x => x.InternalID).Select(x => x.OrderBy(y => y.InternalID).First());

            foreach (CygniaShopifyDespatch o in orders)
            {
                List<CygniaShopifyDespatch> details = desps.FindAll(x => x.InternalID == o.InternalID);
                int OrderID = Convert.ToInt32(o.ShipID.Replace(o.ShipID.Substring(0, 3), ""));

                OrderConfirm.Fulfillment ord = new OrderConfirm.Fulfillment()
                {
                    tracking_company = o.Carrier,
                    tracking_url = GetTrackingLink(o),
                    tracking_number = o.TrackID,
                    location_id = a.LocationID
                };

                List<OrderConfirm.LineItem> lines = new List<OrderConfirm.LineItem>();

                foreach (CygniaShopifyDespatch line in details)
                {
                    OrderConfirm.LineItem l1 = new OrderConfirm.LineItem()
                    {
                        id = line.ItemID,
                        quantity = line.Quantity
                    };

                    lines.Add(l1);
                }

                ord.line_items = lines;

                string ar = UploadOrdersToShopify(a, log, ord, o);
            }



            return cons;
        }


        public static List<CygniaShopifyDespatch> UploadOrdersGet(API a, ILogger log)
        {
            List<CygniaShopifyDespatch> desps = new List<CygniaShopifyDespatch>();

            using (SqlConnection con = new SqlConnection(Environment.GetEnvironmentVariable("ConnScale")))
            {
                con.Open();

                using (SqlCommand comm = con.CreateCommand())
                {
                    int leftInt = 25;
                    if (a.URL.Length < 25)
                    {
                        leftInt = a.URL.Length;
                    }

                    comm.CommandText = a.DespatchSP;
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    comm.Parameters.AddWithValue("@COMPANY", a.Client);
                    comm.Parameters.AddWithValue("@URL", a.URL.Substring(0, leftInt));

                    SqlDataReader despReader = comm.ExecuteReader();

                    while (despReader.Read())
                    {
                        CygniaShopifyDespatch d = new CygniaShopifyDespatch()
                        {
                            ShipID = despReader["ShipID"].ToString(),
                            OrderID = despReader["OrderID"].ToString(),
                            InternalID = Convert.ToInt32(despReader["InternalID"].ToString().Replace(".00000", "")),
                            Weight = Convert.ToDecimal(despReader["TOTAL_WEIGHT"].ToString().Replace(".00000", "")),
                            TrackID = despReader["TrackID"].ToString(),
                            Carrier = despReader["CARRIER"].ToString(),
                            ShipPostCode = despReader["ShipPostCode"].ToString(),
                            Item = despReader["ITEM"].ToString(),
                            ItemID = despReader["ItemID"].ToString(),
                            Quantity = Convert.ToInt32(despReader["Quantity"].ToString().Replace(".00000", ""))
                        };
                        desps.Add(d);
                    }

                    despReader.Close();
                    despReader = null;
                }

            }

            return desps;

        }

        public static void CygniaBaton(string AppName, DateTime? DateStarted, DateTime? DateEnded, DateTime? ExpectedNext, int WarningStatus, ILogger log)
        {
            Baton b = new Baton();
            Batonupdate bu = new Batonupdate();
            bu.AppName = AppName;
            if (DateStarted != null)
            {
                bu.LastStarted = Convert.ToDateTime(DateStarted);
            }
            if (DateEnded != null)
            {
                bu.LastEnded = Convert.ToDateTime(DateEnded);
            }
            if (ExpectedNext != null)
            {
                bu.ExpectedNext = Convert.ToDateTime(ExpectedNext);
            }

            bu.WarningStatus = WarningStatus;
            b.BatonUpdate = bu;

            try
            {

                string postData = JsonSerializer.Serialize<Baton>(b);
                byte[] bytes = Encoding.UTF8.GetBytes(postData);

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                WebRequest request = WebRequest.Create(@"https://cygnia-int-webhooks.azurewebsites.net/api/BatonStatus?code=leQOnt4KGFUfEMbcZmrNdy1LoA9PUuOm8m7a/8ka6cwZQxZO3VmyOA==");
                request.Method = "POST";

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Count());
                }
                var response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                log.LogInformation("CygniaBaton: " + ex.Message);
            }


        }

        [FunctionName("Download")]
        public static void RunDownload([TimerTrigger("%TriggerTimeRunDownload%")] TimerInfo myTimer, ILogger log)
        {

            //PingTest(log);
            NetworkTest(log);

            //CygniaBaton("ShopifyDownload", DateTime.Now, null, null, 2, log);

            List<API> apis = new List<API>();
            Countries = new List<Country>();

            string strGlobeConn = Environment.GetEnvironmentVariable("ConnGlobe");
            //log.LogInformation(strGlobeConn);

            try
            {
                using (SqlConnection con = new SqlConnection(strGlobeConn))
                {
                    log.LogInformation("Connecting: {0}", con.Database);
                    con.Open();
                    log.LogInformation("Connected");

                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "API_SHOPIFY";
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@ENVIRONMENT", Environment.GetEnvironmentVariable("CygniaEnvironment"));

                        SqlDataReader apiReader = comm.ExecuteReader();

                        while (apiReader.Read())
                        {
                            API a = new API()
                            {
                                Client = apiReader["Company"].ToString(),
                                Prefix = apiReader["Prefix"].ToString(),
                                URL = apiReader["URL"].ToString(),
                                APIKey = apiReader["Key"].ToString(),
                                APIPassword = apiReader["Password"].ToString(),
                                APISharedSecret = apiReader["SharedSecret"].ToString(),
                                APICategory9 = apiReader["Category9"].ToString(),
                                LastRun = Convert.ToDateTime(apiReader["LastRun"]),
                                TimeStampID = apiReader["LookupID"].ToString(),
                                ID = Convert.ToInt32(apiReader["ID"].ToString().Replace(".00000", "")),
                                NextShip = Convert.ToDateTime(apiReader["NextShip"].ToString())
                            };

                            //a.LastRun = DateTime.Now.AddMonths(-3);

                            apis.Add(a);
                        }

                        apiReader.Close();

                    }


                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "API_CARRIERS";
                        comm.CommandType = System.Data.CommandType.StoredProcedure;

                        foreach (API a in apis)
                        {
                            comm.Parameters.Clear();
                            comm.Parameters.AddWithValue("@COMPANY", a.Client);

                            SqlDataReader carrierReader = comm.ExecuteReader();

                            List<Carrier> cars = new List<Carrier>();

                            while (carrierReader.Read())
                            {
                                Carrier c = new Carrier()
                                {
                                    APICarrier = carrierReader["APICarrier"].ToString(),
                                    CarrierName = carrierReader["CarrierName"].ToString(),
                                    CarrierService = carrierReader["CarrierService"].ToString(),
                                    CarrierType = carrierReader["CarrierType"].ToString()

                                };

                                double dblThreshold = 0;
                                double.TryParse(carrierReader["CarrierValueThreshold"].ToString(), out dblThreshold);
                                c.CarrierValueThreshold = (dblThreshold == 0 ? 99999999 : dblThreshold);

                                cars.Add(c);
                            }
                            carrierReader.Close();
                            carrierReader = null;
                            a.Carriers = cars;
                        }

                    }
                    //exec API_CARRIERS 'Molton Brown'


                    con.Close();

                }
                DateTime NextRun = DateTime.Now.AddMinutes(15);// myTimer.ScheduleStatus.Next;
                NextRun = DateTime.Now.AddMinutes(15);
                //CygniaBaton("ShopifyDownload", null, DateTime.Now, NextRun, 0, log);

            }
            catch (Exception ex)
            {
                log.LogInformation("RunDownload: " + ex.Message);
            }
            finally
            {

            }

            foreach (API a in apis)
            {
                DateTime LastRun = DateTime.Now;

                log.LogInformation(a.Client);
                log.LogInformation(a.URL);
                DownloadProcessOrders(a, log);
                UpdateTimeStamps(a, log, LastRun);
            }


            //log.LogInformation($"C# Timer trigger function executed at: {DateTime.Now}");
        }

        public static void GetCountries(ILogger log)
        {
            if (Countries.Count == 0)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(Environment.GetEnvironmentVariable("ConnGlobe")))
                    {
                        con.Open();

                        using (SqlCommand comm = con.CreateCommand())
                        {
                            comm.CommandText = "API_COUNTRIES";
                            comm.CommandType = System.Data.CommandType.StoredProcedure;

                            SqlDataReader objCountriesReader = comm.ExecuteReader();

                            while (objCountriesReader.Read())
                            {
                                Country c = new Country()
                                {
                                    CountryCode = objCountriesReader["CountryCode"].ToString(),
                                    CountryName = objCountriesReader["CountryName"].ToString(),
                                    EU = objCountriesReader["EU"].ToString()
                                };

                                Countries.Add(c);
                            }
                        }
                        con.Close();
                    }
                }
                catch (Exception ex)
                {
                    log.LogInformation("UpdateTimeStamps: " + ex.ToString());
                }
            }


        }
        public static void UpdateTimeStamps(API a, ILogger log, DateTime d)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Environment.GetEnvironmentVariable("ConnGlobe")))
                {
                    con.Open();

                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "UPDATE_RUN_TIME";
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@JOB", a.TimeStampID);
                        comm.Parameters.AddWithValue("@RUN_TIME", d);
                        comm.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                log.LogInformation("UpdateTimeStamps: " + ex.ToString());
            }

        }

        public static void DownloadGetAlreadyDownloaded(API a, ILogger log)
        {
            OrdersAlreadyDownloaded = new List<string>();
            string strScaleConn = Environment.GetEnvironmentVariable("ConnScale");

            try
            {
                using (SqlConnection con = new SqlConnection(strScaleConn))
                {
                    log.LogInformation("Connecting: {0}", con.Database);
                    con.Open();
                    log.LogInformation("Connected");

                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "CUSTOM_API_SHOPIFY_ALREADYRECEIVED";
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@COMPANY", a.Client);

                        SqlDataReader ordReader = comm.ExecuteReader();

                        while (ordReader.Read())
                        {
                            string ship = ordReader["SHIPMENT_ID"].ToString();

                            OrdersAlreadyDownloaded.Add(ship);
                        }

                        ordReader.Close();

                    }

                    con.Close();

                }

            }
            catch (Exception ex)
            {
                log.LogInformation("DownloadGetAlreadyDownloaded: " + ex.Message);
            }
            finally
            {

            }
        }

        public static void DownloadProcessOrders(API a, ILogger log)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            WebRequest request = WebRequest.Create(URL(a, "OrderCount", 0));
            //// Set the credentials.
            request.Credentials = new NetworkCredential(a.APIKey, a.APIPassword);

            log.LogInformation(request.RequestUri.AbsoluteUri);

            HttpWebResponse response = null;
            try
            {
                // This is where the HTTP GET actually occurs.
                response = (HttpWebResponse)request.GetResponse();

                int intOrders = OrderCount(response, log);

                if (intOrders > 0)
                {
                    GetCountries(log);
                    DownloadGetAlreadyDownloaded(a, log);
                    DownloadDownloadOrders(a, intOrders, log);
                }

                log.LogInformation("Orders: " + intOrders.ToString());

            }
            catch (Exception ex)
            {
                log.LogInformation("DownloadProcessOrders: " + ex.ToString());
            }
            // Display the status. You want to see "OK" here.



        }

        public static void DownloadDownloadOrders(API a, int intOrders, ILogger log)
        {
            decimal totalpage = 1;

            string strOrderpull = a.LastRun.AddDays(-1).ToString("yyyy-MM-ddTHH:mm:ss");

            if (intOrders > 250)
            {
                totalpage = Math.Ceiling(Convert.ToDecimal(intOrders) / 250);
            }

            for (int page = 1; page <= totalpage; page++)
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                WebRequest request = WebRequest.Create(URL(a, "Orders", page));
                request.Credentials = new NetworkCredential(a.APIKey, a.APIPassword);

                // Get the response.
                HttpWebResponse response = null;
                try
                {
                    // This is where the HTTP GET actually occurs.
                    response = (HttpWebResponse)request.GetResponse();

                    ShopifyOrderCollection orders = DeserialiseOrders(response);

                    DownloadSendOrdersToScale(orders, a, log);



                }
                catch (Exception ex)
                {
                    Console.WriteLine("DownloadDownloadOrders: " + ex.ToString());
                }
            }
        }

        public static void NetworkTest(ILogger log)
        {
            try
            {
                string strNetworkTest = Environment.GetEnvironmentVariable("NetworkTestEnabled");
                if (strNetworkTest == "Yes")
                {
                    string remoteUri = Environment.GetEnvironmentVariable("NetworkAddressTest");
                    //string fileName = "error.html", StringWebResource = null;

                    HttpClient client = new HttpClient();
                    var response = client.GetAsync(remoteUri);

                    log.LogInformation("Network Test. Address: {0} Result: {1}", remoteUri, response.Result.StatusCode.ToString());
                }
                else
                {
                    log.LogInformation("Network Test. Disabled");
                }
            }
            catch (Exception ex)
            {
                log.LogInformation("Network Test: Failed - " + ex.Message);
            }

        }

        public static void PingTest(ILogger log)
        {
            try
            {
                for (int i = 0; i < 4; i++)
                {
                    Ping pingSender = new Ping();
                    PingOptions options = new PingOptions();

                    // Create a buffer of 32 bytes of data to be transmitted.
                    string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                    byte[] buffer = Encoding.ASCII.GetBytes(data);
                    int timeout = 120;
                    string strIP = Environment.GetEnvironmentVariable("PingAddress");

                    log.LogInformation("Ping Test Attempt {0}: {1}", i.ToString(), strIP);

                    PingReply reply = pingSender.Send(strIP, timeout, buffer, options);
                    if (reply.Status == IPStatus.Success)
                    {
                        log.LogInformation("Ping Test: Address: {0} Ping Time: {1}", reply.Address.ToString(), reply.RoundtripTime);
                        //log.LogInformation("Ping Test: RoundTrip time: {0}",);
                    }
                    else
                    {
                        log.LogInformation("Ping Failed. Status: {0} IP: {1}", reply.Status.ToString(), strIP);
                    }
                }
            }
            catch (Exception ex)
            {
                log.LogInformation("PingTest(): - Ping Failed: " + ex.Message);
            }
        }


        public static string GetCarrier(API a, string APICarrier, string RequiredValue, double OrderValue, string strCountry, string strPostCode)
        {

            string strReturn = APICarrier;
            string strCountryType = "";

            Carrier c = null;
            c = a.Carriers.Find(x => x.APICarrier == APICarrier);

            if (c != null)
            {
                switch (RequiredValue)
                {
                    case "Name":
                        strReturn = c.CarrierName;
                        break;
                    case "Service":
                        strReturn = c.CarrierService;
                        break;
                    default:
                        break;
                }
            }

            if (strReturn == APICarrier)
            {
                if (strCountry != "UK" && strCountry != "GB")
                {
                    Country co = null;
                    co = Countries.Find(x => x.CountryCode == strCountry);

                    if (co != null)
                    {
                        strCountryType = co.EU;
                        APICarrier = co.EU;

                        c = a.Carriers.Find(x => x.APICarrier == APICarrier);

                        if (c != null)
                        {
                            switch (RequiredValue)
                            {
                                case "Name":
                                    strReturn = c.CarrierName;
                                    break;
                                case "Service":
                                    strReturn = c.CarrierService;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            if (strReturn == APICarrier || (strPostCode.ToUpper().StartsWith("JE") || strPostCode.ToUpper().StartsWith("GY") || strPostCode.ToUpper().StartsWith("IM")))
            {

                        APICarrier = "CI";

                        c = a.Carriers.Find(x => x.APICarrier == APICarrier);

                        if (c != null)
                        {
                            switch (RequiredValue)
                            {
                                case "Name":
                                    strReturn = c.CarrierName;
                                    break;
                                case "Service":
                                    strReturn = c.CarrierService;
                                    break;
                                default:
                                    break;
                            }
                        }

            }

            return strReturn;
        }

        public static DateTime? ShipDate(DateTime? strDate, string ShipOption)
        {
            if (ShipOption == "Previous Day")
            {
                if (strDate.Value.DayOfWeek == DayOfWeek.Sunday)
                {
                    strDate = strDate.Value.AddDays(-2).Date;
                }
                else if (strDate.Value.DayOfWeek == DayOfWeek.Monday)
                {
                    strDate = strDate.Value.AddDays(-3).Date;
                }
                else
                {
                    strDate = strDate.Value.AddDays(-1).Date;
                }
            }

            if (ShipOption == "2 Day")
            {
                switch (Convert.ToDateTime(strDate).DayOfWeek)
                {
                    case DayOfWeek.Thursday:
                        strDate = Convert.ToDateTime(strDate).AddDays(4);
                        break;
                    case DayOfWeek.Friday:
                        strDate = Convert.ToDateTime(strDate).AddDays(4);
                        break;
                    case DayOfWeek.Saturday:
                        strDate = Convert.ToDateTime(strDate).AddDays(3);
                        break;
                    default:
                        strDate = Convert.ToDateTime(strDate).AddDays(2);
                        break;
                }
            }


            if (ShipOption == "3 Day")
            {
                switch (Convert.ToDateTime(strDate).DayOfWeek)
                {
                    case DayOfWeek.Wednesday:
                        strDate = Convert.ToDateTime(strDate).AddDays(5);
                        break;
                    case DayOfWeek.Thursday:
                        strDate = Convert.ToDateTime(strDate).AddDays(5);
                        break;
                    case DayOfWeek.Friday:
                        strDate = Convert.ToDateTime(strDate).AddDays(5);
                        break;
                    case DayOfWeek.Saturday:
                        strDate = Convert.ToDateTime(strDate).AddDays(4);
                        break;
                    default:
                        strDate = Convert.ToDateTime(strDate).AddDays(3);
                        break;
                }
            }

            if (ShipOption == "Next Day")
            {
                switch (Convert.ToDateTime(strDate).DayOfWeek)
                {
                    case DayOfWeek.Friday:
                        strDate = Convert.ToDateTime(strDate).AddDays(3);
                        break;
                    case DayOfWeek.Saturday:
                        strDate = Convert.ToDateTime(strDate).AddDays(2);
                        break;
                    default:
                        strDate = Convert.ToDateTime(strDate).AddDays(1);
                        break;
                }
            }

            if (ShipOption == "Same Day")
            {
                switch (Convert.ToDateTime(strDate).DayOfWeek)
                {
                    case DayOfWeek.Saturday:
                        strDate = Convert.ToDateTime(strDate).AddDays(2);
                        break;
                    case DayOfWeek.Sunday:
                        strDate = Convert.ToDateTime(strDate).AddDays(1);
                        break;
                    default:
                        strDate = Convert.ToDateTime(strDate).AddDays(0);
                        break;
                }
            }

            return strDate;
        }

        public static CygniaOrders.OrdersCollection DownloadSendOrdersToScaleBoucleme(API a, ShopifyOrderCollection shopifyOrders, CygniaOrders.OrdersCollection cygniaOrders, ILogger log)
        {
            // Custom Requirements
            // carrier = Custom - lookup against a different value
            // change flag for Wholesale

            List<BouclemeVirtualItem> BouclemeVirtualItems = new List<BouclemeVirtualItem>();

            string strScaleConn = Environment.GetEnvironmentVariable("ConnScale");
            //log.LogInformation(strGlobeConn);

            try
            {
                using (SqlConnection con = new SqlConnection(strScaleConn))
                {
                    con.Open();
                    using (SqlCommand comm = con.CreateCommand())
                    {
                        comm.CommandText = "CUSTOM_BOUCLEME_VIRTUALITEMS";
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader read = comm.ExecuteReader();

                        while (read.Read())
                        {
                            BouclemeVirtualItem b = new BouclemeVirtualItem()
                            {
                                Item = read["item"].ToString()
                            };

                            BouclemeVirtualItems.Add(b);
                        }

                    }

                    con.Close();
                }

                foreach (CygniaOrders.Order cygniaOrder in cygniaOrders.Orders)
                {
                    string strOrder = cygniaOrder.Header.SHIPMENT_ID.Replace(a.Prefix, "");
                    int intOrder = Convert.ToInt32(strOrder.Replace(".00000", ""));

                    //Carrier Check
                    OrderList shopifyOrder = shopifyOrders.orders.Find(x => x.order_number == intOrder);

                    if (shopifyOrder != null)
                    {
                        if (shopifyOrder.shipping_lines.Count > 0)
                        {
                            if (shopifyOrder.shipping_lines[0].code.ToUpper() == "CUSTOM")
                            {
                                int yyyyy = 0;

                                string strCarrier = GetCarrier(a, (shopifyOrder.shipping_lines.Count > 0 ? shopifyOrder.shipping_lines[0].title : "No Carrier"), "Name", Convert.ToDouble(shopifyOrder.total_price), shopifyOrder.shipping_address.country_code, shopifyOrder.shipping_address.zip);
                                string strCarrierService = GetCarrier(a, (shopifyOrder.shipping_lines.Count > 0 ? shopifyOrder.shipping_lines[0].title : "No Carrier"), "Service", Convert.ToDouble(shopifyOrder.total_price), shopifyOrder.shipping_address.country_code, shopifyOrder.shipping_address.zip);

                                cygniaOrder.Header.CARRIER = strCarrier;
                                cygniaOrder.Header.CARRIER_SERVICE = strCarrierService;

                                string strDateType = "";

                                if (DateTime.Now.AddMinutes(-30).Hour < 12)
                                {
                                    strDateType = "Next Day";
                                }
                                else
                                {
                                    strDateType = "2 Day";
                                }

                                cygniaOrder.Header.PLANNED_SHIP_DATE = ShipDate(DateTime.Now.AddMinutes(-30).Date, strDateType);
                                cygniaOrder.Header.SCHEDULED_SHIP_DATE = ShipDate(DateTime.Now.AddMinutes(-30).Date, strDateType);

                            }
                        }

                        string strManualOrderType = shopifyOrder.tags;

                        if (strManualOrderType.ToUpper().Contains("WHOLESALE"))
                        {
                            cygniaOrder.Header.CUSTOMER_CATEGORY9 = "";
                            cygniaOrder.Header.ORDER_TYPE = "B2B";


                        }

                    }


                    foreach (CygniaOrders.OrderLine ol in cygniaOrder.Lines)
                    {
                        BouclemeVirtualItem v = BouclemeVirtualItems.Find(x => x.Item == ol.ITEM);

                        if (v != null)
                        {
                            ol.USER_DEF3 = "Virtual";
                            ol.USER_DEF2 = ol.TOTAL_QTY.ToString();
                            ol.TOTAL_QTY = 0;
                            
                        }

                    }

                }
            }
            catch (Exception ex)
            {

            }

            return cygniaOrders;
        }

        public static void DownloadSendOrdersToScale(ShopifyOrderCollection orders, API a, ILogger log)
        {
            List<Promo> promos = new List<Promo>();

            CygniaOrders.OrdersCollection os = new CygniaOrders.OrdersCollection()
            {
                Orders = new CygniaOrders.OrdersCollection.OrdersType()
            };



            promos.Clear();
            string strGlobeConn = Environment.GetEnvironmentVariable("ConnGlobe");
            SqlConnection objConnPromo = new SqlConnection(strGlobeConn);
            objConnPromo.Open();

            SqlCommand objCommand = objConnPromo.CreateCommand();
            objCommand.CommandText = "select * from generic_config_detail where record_type = 'SHOPIFY_PROMO'";

            SqlDataReader objReadPromo = objCommand.ExecuteReader();

            while (objReadPromo.Read())
            {
                Promo p = new Promo()
                {
                    Company = objReadPromo["value"].ToString().Trim(),
                    SkuReceived = objReadPromo["identifier1"].ToString().Trim(),
                    SKUAdd = objReadPromo["identifier2"].ToString().Trim(),
                    QtyAdd = Convert.ToInt32(objReadPromo["identifier3"].ToString().Trim()),
                    DateFrom = objReadPromo["identifier4"].ToString().Trim(),
                    DateTo = objReadPromo["identifier5"].ToString().Trim(),
                    PromoLimit = Convert.ToInt32(objReadPromo["identifier6"].ToString().Trim()),
                    PromoCount = Convert.ToInt32(objReadPromo["identifier7"].ToString().Trim()),
                    ShopifyID = objReadPromo["identifier8"].ToString().Trim(),
                };

                promos.Add(p);
            }

            objReadPromo.Close();
            objReadPromo = null;

            objCommand = null;

            objConnPromo.Close();
            objConnPromo = null;


            string strShipmentID = "";
            bool PromoAdded = false;

            foreach (OrderList o in orders.orders)
            {
                int cnt = 1;
                log.LogInformation("Order Number: " + o.order_number.ToString());

                strShipmentID = a.Prefix + o.order_number.ToString();
                string strAlready = OrdersAlreadyDownloaded.Find(x => x == strShipmentID);

                //
                //  o.shipping_address != null check put in place following implementation of Boucleme, will simply ignore for now
                //

                if (o.shipping_address == null)
                {
                    log.LogInformation("{0}: No Shipping Address Supplied", strShipmentID);
                }

                if (strAlready == null && o.shipping_address != null)
                {

                    string strCarrier = GetCarrier(a, (o.shipping_lines.Count > 0 ? o.shipping_lines[0].code : "No Carrier"), "Name", Convert.ToDouble(o.total_price), o.shipping_address.country_code, o.shipping_address.zip);
                    string strCarrierService = GetCarrier(a, (o.shipping_lines.Count > 0 ? o.shipping_lines[0].code : "No Carrier"), "Service", Convert.ToDouble(o.total_price), o.shipping_address.country_code, o.shipping_address.zip);
                    PromoAdded = false;

                    CygniaOrders.Order order = new CygniaOrders.Order()
                    {
                        Header = new CygniaOrders.Order.HeaderType()
                        {
                            INTERFACE_RECORD_ID = a.Prefix + o.order_number.ToString(),
                            INTERFACE_ACTION_CODE = "NEW",
                            SHIPMENT_ID = a.Prefix + o.order_number.ToString(),
                            SHIP_TO_ADDRESS1 = o.shipping_address.address1,
                            SHIP_TO_ADDRESS2 = o.shipping_address.address2,
                            //SHIP_TO_ADDRESS3 = o.shipping_address.province.ToString(),
                            SHIP_TO_CITY = o.shipping_address.city,
                            SHIP_TO_NAME = o.shipping_address.first_name + " " + o.shipping_address.last_name,
                            SHIP_TO_POSTAL_CODE = o.shipping_address.zip,
                            SHIP_TO_COUNTRY = o.shipping_address.country_code,
                            SHIP_TO_PHONE_NUM = o.shipping_address.phone,
                            SHIP_TO_EMAIL_ADDRESS = o.email,
                            REQUESTED_DELIVERY_TYPE = "by",
                            SCHEDULED_SHIP_DATE = a.NextShip,
                            PLANNED_SHIP_DATE = a.NextShip,
                            WAREHOUSE = "Dalepak",
                            COMPANY = a.Client,
                            CARRIER = strCarrier,
                            CARRIER_SERVICE = strCarrierService,
                            CUSTOMER_CATEGORY9 = a.APICategory9,
                            CONSOLIDATION_ALLOWED = "N",
                            USER_STAMP = "AzureFunction",
                            ERP_ORDER = o.id.ToString(),
                            USER_DEF1 = "SHOPIFY",
                            USER_DEF2 = (a.URL.Length > 25 ? a.URL.Substring(0, 25) : a.URL),
                            USER_DEF3 = o.currency,
                            USER_DEF4 = a.ID.ToString(),
                            USER_DEF7 = Convert.ToDecimal(o.subtotal_price),
                            ORDER_TYPE = "B2C",

                            HeaderComments = new CygniaOrders.Order.HeaderType.HeaderCommentsType(),
                        },
                        Lines = new CygniaOrders.Order.LinesType(),
                    };
                    log.LogInformation("{0}: Header Added", strShipmentID);

                    if (o.note != null)
                    {
                        if (o.note.Length > 0)
                        {
                            CygniaOrders.OrderComment oc = new CygniaOrders.OrderComment()
                            {
                                COMMENT_TYPE = "GIFT",
                                INTERFACE_RECORD_ID = order.Header.INTERFACE_RECORD_ID + "GIFT",
                                INTERFACE_ACTION_CODE = "NEW",
                                TEXT = o.note,
                                INTERFACE_LINK_ID = order.Header.INTERFACE_RECORD_ID
                            };
                            order.Header.HeaderComments.Add(oc);
                        }
                    }

                    foreach (LineItem it in o.line_items)
                    {

                        cnt++;
                        //TotalValue = TotalValue + (Convert.ToDecimal(i.price) * Convert.ToDecimal(i.quantity));

                        CygniaOrders.OrderLine ol = new CygniaOrders.OrderLine()
                        {
                            INTERFACE_RECORD_ID = a.Prefix + o.order_number.ToString() + cnt.ToString(),
                            INTERFACE_ACTION_CODE = "NEW",
                            ERP_ORDER_LINE_NUM = cnt,
                            ITEM = it.sku,
                            ITEM_DESC = it.name,
                            SHIPMENT_ID = a.Prefix + o.order_number.ToString(),
                            INTERFACE_LINK_ID = a.Prefix + o.order_number.ToString(),
                            REQUESTED_QTY = Convert.ToDecimal(it.quantity),
                            TOTAL_QTY = Convert.ToDecimal(it.quantity),
                            COMPANY = order.Header.COMPANY,
                            VALUE = Convert.ToDecimal(it.price),
                            ITEM_LIST_PRICE = Convert.ToDecimal(it.price),
                            ITEM_NET_PRICE = Convert.ToDecimal(it.price),
                            USER_DEF1 = it.id.ToString(),
                            INVOICE = o.order_number.ToString(),
                            LineComments = new CygniaOrders.OrderLine.LineCommentsType(),
                        };
                        order.Lines.Add(ol);

                        log.LogInformation("{0}: Item Added", strShipmentID);

                        try
                        {
                            Promo p1 = promos.Find(x => x.SkuReceived == it.sku && x.Company == order.Header.COMPANY && x.ShopifyID == order.Header.USER_DEF4);

                            if (p1 != null)
                            {
                                if (DateTime.Now > DateTime.ParseExact(p1.DateFrom, "dd/MM/yyyy",null) && DateTime.Now < DateTime.ParseExact(p1.DateTo, "dd/MM/yyyy", null))
                                {
                                    CygniaOrders.OrderLine ol1 = new CygniaOrders.OrderLine()
                                    {

                                        INTERFACE_RECORD_ID = a.Prefix + o.order_number.ToString() + cnt.ToString() + "P",
                                        INTERFACE_ACTION_CODE = "NEW",
                                        ERP_ORDER_LINE_NUM = Convert.ToDecimal(cnt + 0.1),
                                        ITEM = p1.SKUAdd,
                                        SHIPMENT_ID = a.Prefix + o.order_number.ToString(),
                                        INTERFACE_LINK_ID = a.Prefix + o.order_number.ToString(),
                                        REQUESTED_QTY = p1.QtyAdd,
                                        TOTAL_QTY = p1.QtyAdd,
                                        ITEM_DEPARTMENT = "PROMO-" + p1.SKUAdd,
                                        COMPANY = order.Header.COMPANY,

                                        LineComments = new CygniaOrders.OrderLine.LineCommentsType(),
                                    };
                                    order.Lines.Add(ol1);
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            log.LogInformation("{0}: Error 1 {1}", strShipmentID, ex.Message);
                        }

                    }

                    try
                    {
                        List<Promo> p2 = promos.FindAll(x => x.SkuReceived == "All" && x.Company == order.Header.COMPANY && x.ShopifyID == order.Header.USER_DEF4);

                        if (p2 != null)
                        {
                            double erpIncrement = 0.1;

                            int PromoLine = 1;
                            foreach (Promo det in p2)
                            {
                                if (DateTime.Now > DateTime.ParseExact(det.DateFrom, "dd/MM/yyyy", null) && DateTime.Now < DateTime.ParseExact(det.DateTo, "dd/MM/yyyy", null) && PromoAdded == false)
                                {
                                    CygniaOrders.OrderLine ol2 = new CygniaOrders.OrderLine()
                                    {

                                        INTERFACE_RECORD_ID = a.Prefix + o.order_number.ToString() + PromoLine + "PA",
                                        INTERFACE_ACTION_CODE = "NEW",
                                        ERP_ORDER_LINE_NUM = Convert.ToDecimal(cnt + erpIncrement),
                                        ITEM = det.SKUAdd,
                                        SHIPMENT_ID = a.Prefix + o.order_number.ToString(),
                                        INTERFACE_LINK_ID = a.Prefix + o.order_number.ToString(),
                                        REQUESTED_QTY = det.QtyAdd,
                                        TOTAL_QTY = det.QtyAdd,
                                        ITEM_DEPARTMENT = "PROMO-" + det.SKUAdd,
                                        COMPANY = order.Header.COMPANY,

                                        LineComments = new CygniaOrders.OrderLine.LineCommentsType(),
                                    };
                                    order.Lines.Add(ol2);
                                    PromoLine++;
                                    erpIncrement = erpIncrement + 0.1;
                                }
                            }
                            PromoAdded = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        log.LogInformation("{0}: Error 2 {1}", strShipmentID, ex.Message);
                    }

                    os.Orders.Add(order);
                }
                else
                {
                    log.LogInformation("Download Orders: " + strShipmentID + " already downloaded");
                }

            }
            if (os.Orders.Count > 0)
            {
                // Check to see if any custom code is implemented

                os = DownloadSendOrdersToScaleCustom(a, orders, os, log);
                // Remove the above for now so that the Boucleme logic isn't implemented yet

                CygniaOrders.OrderImportsClient client = new CygniaOrders.OrderImportsClient();
                CygniaOrders.OrdersCollection res = client.CreateOrderAsync(os).Result;

            }
            else
            {
                log.LogInformation("Download Orders: Nothing to download after removing already processed");
            }

        }

        public static CygniaOrders.OrdersCollection DownloadSendOrdersToScaleCustom(API a, ShopifyOrderCollection shopifyOrders, CygniaOrders.OrdersCollection cygniaOrders, ILogger log)
        {

            var st = new StackTrace();
            var sf = st.GetFrame(0);

            var currentMethodName = sf.GetMethod();
            string currentNameSpace = currentMethodName.DeclaringType.ToString();
            string strMethod = currentMethodName.Name.Replace("Custom", "") + a.Client;

            System.Type myType = System.Type.GetType(currentNameSpace);

            object[] oParams = new object[4];
            oParams[0] = a;
            oParams[1] = shopifyOrders;
            oParams[2] = cygniaOrders;
            oParams[3] = log;

            cygniaOrders = InvokeMethod(myType, strMethod, oParams);

            return cygniaOrders;
        }

        static CygniaOrders.OrdersCollection InvokeMethod(System.Type myType, string myMethodName, object[] parameters)
        {
            object o = parameters[2];
            API a = (API)parameters[0];

            ILogger log = (ILogger)parameters[3];

            MethodInfo m = myType.GetMethod(myMethodName);
            if (m != null)
            {
                log.LogInformation("Custom code found for: {0}", a.Client);
                o = m.Invoke(null, parameters);
            } else
            {
                log.LogInformation("No custom code found for: {0}", a.Client);
            }

            return (CygniaOrders.OrdersCollection)o;
        }

        public static ShopifyOrderCollection DeserialiseOrders(HttpWebResponse response)
        {
            ShopifyOrderCollection o = null;

            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();

            if (response.StatusDescription == "OK")
            {
                //StreamWriter w = new StreamWriter(@"C:\Temp\json" + DateTime.Now.Millisecond.ToString());
                //w.WriteLine(responseFromServer);
                //w.Close();
                //w = null;
                o = JsonSerializer.Deserialize<ShopifyOrderCollection>(responseFromServer);


            }

            reader.Close();
            dataStream.Close();
            response.Close();

            return o;
        }

        public static string URL(API a, string action, int intPage, string strParam = "")
        {
            string url = "";
            string strOrderPull = a.LastRun.AddHours(-1).ToString("yyyy-MM-ddTHH:mm:ss");

            switch (action)
            {
                case "OrderCount":
                    url = "https://" + a.URL + "/admin/orders/count.json?updated_at_min=" + strOrderPull + "&status=open";
                    break;
                case "Orders":
                    url = "https://" + a.URL + "/admin/orders.json?updated_at_min=" + strOrderPull + "&limit=250&status=open";
                    break;
                case "Despatch":
                    url = "https://" + a.URL + "/admin/api/2021-01/orders/" + strParam + @"/fulfillments.json";
                    break;
                case "Products":
                    url = "https://" + a.URL + "/admin/api/2021-01/products.json?limit=" + intPage.ToString();
                    break;
                case "ProductsCount":
                    url = "https://" + a.URL + "/admin/api/2021-01/products/count.json";
                    break;
                case "Stock":
                    url = "https://" + a.URL + "/admin/inventory_levels/set.json";
                    break;
                case "StockAdjust":
                    url = "https://" + a.URL + "/admin/inventory_levels/adjust.json";
                    break;
            }


            return url;
        }

        public class Promo
        {
            public string Company { get; set; }
            public string SkuReceived { get; set; }
            public string SKUAdd { get; set; }
            public int QtyAdd { get; set; }
            public string DateFrom { get; set; }
            public string DateTo { get; set; }
            public int PromoLimit { get; set; }
            public int PromoCount { get; set; }
            public string ShopifyID { get; set; }

        };

        public static int OrderCount(HttpWebResponse response, ILogger log)
        {
            int intCount = 0;

            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();


            if (response.StatusDescription == "OK")
            {
                ShopifyCount count = JsonSerializer.Deserialize<ShopifyCount>(responseFromServer);
                //log.LogInformation(responseFromServer);
                intCount = count.count; //Convert.ToInt32(GoodResp.count.Value);
            }

            reader.Close();
            dataStream.Close();
            response.Close();

            return intCount;
        }


    }
}
